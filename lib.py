import errno
import os

import config


def createFolder(nameWithPath):
    try:
        os.makedirs(nameWithPath)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(nameWithPath):
            pass
        else:
            raise print("Creation of the directory %s failed" %
                        nameWithPath)


def filter(original, exclude):
    index = 0
    temp = original.copy();
    for group in temp:
        if group in exclude or (type(group) is dict and group['name'] in exclude):
            original.pop(index)
        else:
            index += 1


def groupsFilter(originalGroups):
    filter(originalGroups, config.skipGroups)


def projectsFilter(originalProjects):
    filter(originalProjects, config.skipProjects)
