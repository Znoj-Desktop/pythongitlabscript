#!/usr/bin/python3

import cloneOrPull
import commit
import createJsonFiles
import readme
import editImages as edit
import copy
import backup

helpOp = ['help', 'h', '--help', '-h', '0']
helpDesc = '- Print help menu with description.\n'

createOp = ['create', 'json', '1']
createDesc = '- Create file \'groups.json\' with all groups (and details) matching \'urlGroups\' value in \'config.py\'.\n'
createDesc += 'Create file for every group from \'groups.json\' except groups in \'skipGroups\' set in \'config.py\' with all containing projects (max 100).\n'

cloneOp = ['clone', 'pull', '2']
cloneDesc = '- For all projects and groups except those mentioned in \'skipGroups\' variable or \'skipProjects\' variable sets in \'config.py\',\n'
cloneDesc += 'clone repository when not exists or pull if git repository already exists in structure begin with folder \'repoRootPath\' set in \'config.py\',\n'
cloneDesc += 'inside this folder is folder with group name of this project and inside this folder is one with the name of this project containing repository. \n'


commitOp = ['commit', '3']
commitDesc = '- For all projects inside folder set in variable \'repoRootPath\' in \'config.py\',\n'
commitDesc += 'pull all changes from server, detect local changes and commit changes with \'commitMessage\' set in \'config.py\'.\n'

readmeOp = ['readme', '4']
readmeDesc = '- For all projects inside folder set in variable \'repoRootPath\' in \'config.py\' having readme file with name defined in \'config.py\' as \'readmeFileName\',\n'
readmeDesc += 'create folder with name as value of variable \'readmeFolderName\' in \'config.py\' and copy all files mentioned in \'readmeFileName\' into this folder,\n'
readmeDesc += 'rename all these files to point into \'readmeFolderName\' and copy icon with name \'iconName\' defined in \'config.py\' if exists and not copied yet.\n'

editOp = ['edit', 'editImages', 'images', '5']
editDesc = 'Edit \'readmeFileName\' from all projects inside folder set in variable \'repoRootPath\' in \'config.py\'\n'
editDesc += 'to set max size off all images and align it to center.\n'

copyOp = ['copy', '6']
copyDesc = 'Copy \'readmeFolderName\' and \'readmeFileName\' from all projects inside folder set in variable \'repoRootPath\' in \'config.py\'\n'
copyDesc += 'to new folder defined as \'newPublicDir\' in \'config.py\'.\n'
copyDesc += 'All group config files are slightly modified and imported into global file \'groups.js\'. This file is exported as config js file to \'newConfigDir\'.\n'

backupOp = ['backup', '7']
backupDesc = 'Copy projects from \'repoRootPath\' folder to \'newBackupDir\' folder.\n'
backupDesc += 'All folders or files match pattern in \'backupFoldersToSkip\' will be skipped.\n'
backupDesc += 'I don\'t need to backup whole GIT history and \'npm_modules\' folder, so I am using this script.\n'

exitOp = ['exit', 'q', '8']
exitDesc = '- Exit program, can be used only after printing menu as well as other options.\n'

operations = [helpOp, createOp, cloneOp, commitOp, readmeOp, editOp, copyOp, backupOp, exitOp]
operationsDesc = [helpDesc, createDesc,
                  cloneDesc, commitDesc, readmeDesc, editDesc, copyDesc, backupDesc, exitDesc]


def printHelp(desc=False):
    index = 0
    print("########################### options ###########################")
    print('\nChoose one of following options:\n')
    for operation in operations:
        print('{0}. \'{1}\''.format(index, operation[0]), end='')
        for o in operation[1:]:
            print(' or \'{0}\''.format(o), end='')
        print('')
        if desc:
            print(operationsDesc[index])
        index += 1
    if not desc:
        print("\n########################### options ###########################")
        print('Chosen option: ', end='')


userInput = ''
while(userInput not in exitOp):
    if userInput != '':
        print('ENTER for menu...', end='')
        userInput = input()
    printHelp()
    userInput = input()
    if userInput in createOp:
        createJsonFiles.main()
    elif userInput in cloneOp:
        cloneOrPull.main()
    elif userInput in commitOp:
        commit.main()
    elif userInput in readmeOp:
        readme.main()
    elif userInput in editOp:
        edit.main()
    elif userInput in copyOp:
        copy.main()
    elif userInput in backupOp:
        backup.main()
    elif userInput in helpOp:
        printHelp(True)
