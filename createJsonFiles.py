#!/usr/bin/python3

import json
import os

import requests

import config
import lib

###################################### creating file groups.json ##################################

def main():
    groups = []
    projects = []
    groupsFileName = '{0}/{1}.json'.format(config.groupsFilePath, config.groupsFileName)
    # if config.groupsFilePath is not '.':
    lib.createFolder(config.groupsFilePath)

    print('creating file {0}'.format(groupsFileName))
    response = requests.request(
        "GET", config.urlGroups, data=config.payload, headers=config.headers, params=config.querystring)

    jsonGroupsLoad = []
    for element in json.loads(response.text):
        if element['name'] in config.skipGroups:
            print(element['name'] + ' skipped')
        else:
            jsonGroupsLoad.append(element)

    jsonGroupsFormat = json.dumps(jsonGroupsLoad, indent=4, sort_keys=True)

    with open(groupsFileName, 'w') as outfile:
        outfile.write(jsonGroupsFormat)

    groups = [element for element in jsonGroupsLoad]
    lib.groupsFilter(groups)
    for group in groups:
        groupFileName = '{0}/{1}.json'.format(config.groupsFilePath, group['path'])
        print('creating group file {0}'.format(groupFileName))
        response = requests.request(
            "GET", config.urlProjects.format(group['id']), data=config.payload, headers=config.headers, params=config.querystring)

        jsonProjectsLoad = []
        for element in json.loads(response.text):
            if element['name'] in config.skipProjects:
                print(element['name'] + ' skipped')
            else:
                print(element['name'] + ' added')
                jsonProjectsLoad.append(element)

        jsonProjectsFormat = json.dumps(
            jsonProjectsLoad, indent=4, sort_keys=True)
        projects += [element for element in jsonProjectsLoad]
        # change to 'id'?
        with open(groupFileName, 'w') as outfile:
            outfile.write(jsonProjectsFormat)
    print('All files created')


if __name__ == "__main__":
    main()
