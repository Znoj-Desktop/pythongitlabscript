#!/usr/bin/python3

import os
import re
import shutil

import config
import lib

######################################## edit README files #########################################

def main():
    groups = []
    projects = []

    groups = next(os.walk(config.repoRootPath))[1]
    lib.groupsFilter(groups)
    
    if not os.path.exists(config.repoRootPath):
        "Run clone first.\n"
        exit()
        
    for group in groups:
        groupPath = '{0}/{1}'.format(config.repoRootPath, group)
        projects = []
        projects = next(os.walk(groupPath))[1]
        lib.projectsFilter(projects)

        for project in projects:
            projectPath = '{0}/{1}'.format(groupPath, project)
            readmeFile = '{0}/{1}'.format(projectPath,
                                            config.readmeFileName)

            if not os.path.exists(projectPath):
                raise FileNotFoundError(
                    'Project path {0} does not exists!'.format(projectPath))

            if not os.path.exists(readmeFile):
                print('{0} is missing'.format(readmeFile))
            else:
                with open(readmeFile, 'r', encoding="utf8") as infile:
                    content = infile.read()
                content = re.sub(r'\!?\[(.*)\]\(\.?\/?(README.*\.png)\)', 
                "<div align=\"center\">\n" + 
                "  <img src=\"./" + r"\2" + "\" align=\"center\" style=\"max-width:956px; max-height:600px; margin:5px 0 10px 0;\">" + r"\1" + "</img>\n" + 
                "</div>\n", content)
                with open(readmeFile, 'w', encoding="utf8") as outfile:
                    outfile.write(content)

        print('\n Updating group {0} done'.format(group))

if __name__ == "__main__":
    main()
