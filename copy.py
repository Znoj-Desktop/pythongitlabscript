#!/usr/bin/python3

import errno
import os
import shutil
import stat
import time

import config
import lib

######################################## create README structure #########################################

def main():
    groups = []
    projects = []

    groups = next(os.walk(config.repoRootPath))[1]
    lib.groupsFilter(groups)

    if not os.path.exists(config.newPublicDir):
        os.mkdir(config.newPublicDir)
    try:
        for group in groups:
            newGroupPath = '{0}/{1}'.format(config.newPublicDir, group)
            if not os.path.exists(newGroupPath):
                os.mkdir(newGroupPath)

            groupPath = '{0}/{1}'.format(config.repoRootPath, group)
            projects = []
            projects = next(os.walk(groupPath))[1]
            lib.projectsFilter(projects)
            for project in projects:
                print(project)
                newProjectPath = '{0}/{1}'.format(newGroupPath, project)
                if not os.path.exists(newProjectPath):
                    os.mkdir(newProjectPath)

                projectPath = '{0}/{1}'.format(groupPath, project)
                readmeFile = '{0}/{1}'.format(projectPath,
                                              config.readmeFileName)
                newReadmeFile = '{0}/{1}'.format(newProjectPath,
                                              config.readmeFileName)

                readmeFolder = '{0}/{1}'.format(projectPath,
                                                config.readmeFolderName)
                newReadmeFolder = '{0}/{1}'.format(newProjectPath,
                                                config.readmeFolderName)

                if not os.path.exists(projectPath):
                    raise FileNotFoundError(
                        'Project path {0} does not exists!'.format(projectPath))

                if not os.path.exists(readmeFile):
                    print('{0} is missing'.format(readmeFile))
                else:
                    if not os.path.exists(newReadmeFile):
                        print('copy file {0} to {1}\n'.format(readmeFile, newReadmeFile))
                        shutil.copy(readmeFile, newReadmeFile)
                    if not os.path.exists(readmeFolder):
                        print(
                            '{0} is missing.'.format(readmeFolder))
                        continue

                    elif not os.path.isdir(readmeFolder):
                        raise FileNotFoundError(
                            'Unknown error - {0} should be a folder.'.format(readmeFolder))
                            
                    if os.path.exists(newReadmeFolder):
                        print('remove folder {0}\n'.format(newReadmeFolder))
                        shutil.rmtree(newReadmeFolder, ignore_errors=True, onerror=handleRemoveReadonly)
                        time.sleep(1) # due to rmtree error in python, can be more errors when structure is more complex
                    print('copy folder {0} to {1}\n'.format(readmeFolder, newReadmeFolder))
                    shutil.copytree(readmeFolder, newReadmeFolder)

            print('\n Updating group {0} done'.format(group))

    except Exception as exc:
        print('{0} - exc: {1}'.format(projectPath, exc))

    if not os.path.exists(config.newConfigPath):
        os.mkdir(config.newConfigPath)
    if config.groupsFilePath == '.' or os.path.exists(config.groupsFilePath):
        groupsFileNameIn = '{0}/{1}.json'.format(config.groupsFilePath, config.groupsFileName)

        groups = next(os.walk(config.groupsFilePath))[2]
        if not os.path.exists(groupsFileNameIn):
            print('Create config files first!\n')
            exit()

        groups.pop(groups.index('{0}.json'.format(config.groupsFileName)))
        content = ''
        with open(groupsFileNameIn, 'r') as infile:
            content = infile.read()
        
        imports = ''
        for group in groups:
            imports += 'import {{ default as {0} }} from \'./{1}\';\n'.format(group.replace('-', '').replace('.json', ''), group.replace('.json', '.js'))

        content = imports + '\nexport const GitlabGroups = ' + content
        content += '\n\n/* eslint-disable import/no-anonymous-default-export */\nexport default {\n'
        content += '  GitlabGroups,'
        for group in groups:
            groupName = group.replace('.json', '')
            content += '\n  ' + groupName.replace('-', '') + ','
        content = content[:-1] + '\n};'

        if not os.path.exists(config.groupsFilePath):
            os.mkdir(config.groupsFilePath)
        with open('{0}/{1}.js'.format(config.newConfigPath, config.groupsFileName), 'w') as outfile:
            outfile.write(content)

        for group in groups:
            with open('{0}/{1}'.format(config.groupsFilePath, group), 'r') as infile:
                content = infile.read()

            with open('{0}/{1}'.format(config.newConfigPath, group.replace('.json', '.js')), 'w') as outfile:
                outfile.write('/* eslint-disable import/no-anonymous-default-export */\nexport default ' + content)

    else:
        print('Can not find folder {0} with configs.'.format(config.groupsFilePath))
        
def handleRemoveReadonly(func, path, exc):
    excvalue = exc[1]
    if func in (os.rmdir, os.remove) and excvalue.errno == errno.EACCES:
        os.chmod(path, stat.S_IRUSR) # 0777
        func(path)
    else:
        raise

if __name__ == "__main__":
    main()
