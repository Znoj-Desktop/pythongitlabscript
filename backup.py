#!/usr/bin/python3

import errno
import os
import shutil
import stat
import time

import config
import lib

######################################## create BACKUP structure #########################################

def main():
    groups = []
    projects = []

    groups = next(os.walk(config.repoRootPath))[1]
    # lib.groupsFilter(groups) not needed

    if not os.path.exists(config.newBackupDir):
        os.mkdir(config.newBackupDir)
    try:
        for group in groups:
            newGroupPath = '{0}/{1}'.format(config.newBackupDir, group)
            if not os.path.exists(newGroupPath):
                os.mkdir(newGroupPath)

            groupPath = '{0}/{1}'.format(config.repoRootPath, group)
            projects = []
            projects = next(os.walk(groupPath))[1]
            # lib.projectsFilter(projects) not needed
            for project in projects:
                print(project)
                projectPath = '{0}/{1}'.format(groupPath, project)
                if not os.path.exists(projectPath):
                    raise FileNotFoundError(
                        'Project path {0} does not exists!'.format(projectPath))

                newProjectPath = '{0}/{1}'.format(newGroupPath, project)
                if os.path.exists(newProjectPath):
                    print('remove old folder {0}\n'.format(newProjectPath))
                    shutil.rmtree(newProjectPath, ignore_errors=True, onerror=handleRemoveReadonly)
                    time.sleep(1) # due to rmtree error in python, can be more errors when structure is more complex

                print('backup folder {0} to {1}\n'.format(projectPath, newProjectPath))
                shutil.copytree(projectPath, newProjectPath, ignore=shutil.ignore_patterns(*config.backupFoldersToSkip))

            print('\n Updating group {0} done'.format(group))

    except Exception as exc:
        print('{0} - exc: {1}'.format(projectPath, exc))

def handleRemoveReadonly(func, path, exc):
    excvalue = exc[1]
    if func in (os.rmdir, os.remove) and excvalue.errno == errno.EACCES:
        os.chmod(path, stat.S_IRUSR) # 0777
        func(path)
    else:
        raise

if __name__ == "__main__":
    main()
