#!/usr/bin/python3

import os

from git import Repo

import config
import lib


def main():
    groups = []
    projects = []

    groups = next(os.walk(config.repoRootPath))[1]
    lib.groupsFilter(groups)

    for group in groups:
        groupPath = '{0}/{1}'.format(config.repoRootPath, group)
        projects = []
        projects = next(os.walk(groupPath))[1]
        lib.projectsFilter(projects)
        counterAll = len(projects)
        counterCurrent = 1
        for project in projects:
            projectPath = '{0}/{1}'.format(groupPath, project)
            print('Pull and status from repo {0}'.format(projectPath))
            try:
                repo = Repo.init(projectPath)
                origin = repo.remotes.origin
                origin.pull()

                removedOrModifiedFiles = [
                    item.a_path for item in repo.index.diff(None)]
                newFiles = repo.untracked_files

                if removedOrModifiedFiles or newFiles:
                    print('Removed or modified: {0}'.format(
                        removedOrModifiedFiles))
                    print('New: {0}'.format(newFiles))

                    # comment if you know what you are doing
                    # print('ARE YOU SURE? - Do you want to commit and push these changes? y/n')
                    # acceptChanges = input()
                    # if acceptChanges.lower() is 'y':
                    # comment if you know what you are doing
                    repo.git.add('--all')
                    repo.git.commit('-am', config.commitMessage)
                    repo.remotes.origin.push()
                    print('Pushed to repo {0}'.format(projectPath))
                else:
                    print('Nothing changed')
            except Exception as exc:
                print('{0} - exc: {1}'.format(projectPath, exc))

            print('{0}/{1} - {2}% for group {3}'.format(counterCurrent,
                                                        counterAll, round(counterCurrent*100/counterAll, 2), group))
            counterCurrent += 1
        print('\n Updating group {0} done'.format(group))


if __name__ == "__main__":
    main()
