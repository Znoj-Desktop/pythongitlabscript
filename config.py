#!/usr/bin/python3

urlGroups = "https://gitlab.com/api/v4/groups?owned=true"
urlProjects = "https://gitlab.com/api/v4/groups/{0}/projects"
privateToken = "xMXYjm2zWQvHNJ5Zegsx" # put private token from CI in here

querystring = {"private_token": privateToken, "per_page": "100"}

payload = ""
headers = {
    'cache-control': "no-cache"
}

groupsFileName = 'groups'
groupsFilePath = 'config' # . for root

repoRootPath = 'repos'
readmeFileName = 'README.md'
readmeFolderName = 'README'
iconName = 'icon.png'

skipGroups = ['Znoj-rest', 'Znoj-Backend', 'Znoj-Interviews', 'powerful']
skipProjects = ['eLogika', 'repoPlayer', 'powerful']

commitMessage = 'Commit from python script'

newConfigPath = '../gitlabprojects/src/configs'
newPublicDir = '../gitlabprojects/public'

newBackupDir = 'backupRepos'
backupFoldersToSkip = ['.git', 'node_modules']