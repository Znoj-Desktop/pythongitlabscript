#!/usr/bin/python3

import os
import re
import shutil

import config
import lib

######################################## create README structure #########################################


# works only for files in project's root
def copyDependencies(readmeFile, readmeFolder):
    stringsBeforeFile = ['](./', '](/', '](']
    content = ''
    with open(readmeFile, 'r+', encoding="utf8") as inFile:
        content = inFile.read()
        filesFromReadme = re.findall(
            '\]\(\.?\/?((?!https?:|\.?\/?README).*\.[a-z, A-Z, 0-9]*)\)'.format(config.readmeFolderName), str(content))

        for fileFromReadme in filesFromReadme:
            newFileName = '{0}/{1}'.format(config.readmeFolderName,
                                           fileFromReadme)

            print('Replace {0} for {1} in {2}'.format(
                fileFromReadme, newFileName, config.readmeFileName))
            for strBeforeFile in stringsBeforeFile:
                content = content.replace('{0}{1}'.format(
                    strBeforeFile, fileFromReadme), '{0}{1}'.format(strBeforeFile, newFileName))

            readmeFilePath = readmeFile[:(readmeFile.find(
                config.readmeFileName)-1)]
            originalFileFullPath = '{0}/{1}'.format(
                readmeFilePath, fileFromReadme).replace('%20', ' ').replace('%C3%AD', 'í').replace('%C3%BD', 'ý')
            newFileFullPath = '{0}/{1}'.format(readmeFolder, fileFromReadme)
            print('copy {0} to {1}\n'.format(
                originalFileFullPath, newFileFullPath))
            shutil.copy(originalFileFullPath, readmeFolder)
    with open(readmeFile, 'w', encoding="utf8") as outFile:
        outFile.write(content)


def main():
    groups = []
    projects = []

    groups = next(os.walk(config.repoRootPath))[1]
    lib.groupsFilter(groups)

    try:
        for group in groups:
            groupPath = '{0}/{1}'.format(config.repoRootPath, group)
            projects = []
            projects = next(os.walk(groupPath))[1]
            lib.projectsFilter(projects)
            for project in projects:
                projectPath = '{0}/{1}'.format(groupPath, project)
                readmeFile = '{0}/{1}'.format(projectPath,
                                              config.readmeFileName)
                readmeFolder = '{0}/{1}'.format(projectPath,
                                                config.readmeFolderName)

                if not os.path.exists(projectPath):
                    raise FileNotFoundError(
                        'Project path {0} does not exists!'.format(projectPath))

                if not os.path.exists(readmeFile):
                    print('{0} is missing'.format(readmeFile))
                else:
                    if not os.path.exists(readmeFolder):
                        print(
                            '{0} is missing - creating one...'.format(readmeFolder))
                        lib.createFolder(readmeFolder)

                    elif not os.path.isdir(readmeFolder):
                        raise FileExistsError(
                            'File {0} exists! remove it and run this script again.'.format(readmeFolder))
                    copyDependencies(readmeFile, readmeFolder)

                iconPath = '{0}/{1}'.format(projectPath, config.iconName)
                if os.path.exists(iconPath) and not os.path.exists('{0}/{1}'.format(readmeFolder, config.iconName)):
                    print('copy {0} to {1}\n'.format(iconPath, readmeFolder))
                    shutil.copy(iconPath, readmeFolder)

            print('\n Updating group {0} done'.format(group))

    except Exception as exc:
        print('{0} - exc: {1}'.format(projectPath, exc))


if __name__ == "__main__":
    main()
