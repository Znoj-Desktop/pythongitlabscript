#!/usr/bin/python3

import json
import os

import requests
from git import Repo

import config
import lib


def main():
    groups = []
    projects = []
    projectsRepoUrl = []
    ########################################### get groups ############################################

    print('getting all groups...')
    response = requests.request(
        "GET", config.urlGroups, data=config.payload, headers=config.headers, params=config.querystring)

    jsonGroupsLoad = json.loads(response.text)
    jsonGroupsFormat = json.dumps(jsonGroupsLoad, indent=4, sort_keys=True)

    ######################################### get projects ############################################
    groups = [element for element in jsonGroupsLoad]
    lib.groupsFilter(groups)
    for group in groups:
        print('getting projects from group {0}...'.format(group['path']))
        response = requests.request(
            "GET", config.urlProjects.format(group['id']), data=config.payload, headers=config.headers, params=config.querystring)

        jsonProjectsLoad = json.loads(response.text)
        jsonProjectsFormat = json.dumps(
            jsonProjectsLoad, indent=4, sort_keys=True)
        projects += [element for element in jsonProjectsLoad]
        lib.projectsFilter(projects)

    ######################################## creating git repos #######################################
    # projectsRepoUrl = [element['ssh_url_to_repo'] for element in projects]
    # print(projectsRepoUrl)
    # print(projects)
    print('\n Creating or Updating repos...')
    lib.createFolder(config.repoRootPath)

    counterAll = len(projects)
    counterCurrent = 1
    for project in projects:
        repo = project['ssh_url_to_repo']
        localRepoDir = '{0}/{1}'.format(config.repoRootPath,
                                        project['path_with_namespace'])
        try:
            if os.path.isdir(localRepoDir):
                print('Pull from repo {0}'.format(localRepoDir))
                repo = Repo.init(localRepoDir)
                origin = repo.remotes.origin
                origin.pull()
            else:
                print('Clone repo {0}'.format(localRepoDir))
                repo = Repo.clone_from(repo, localRepoDir)
        except Exception as exc:
            print('{0} - exc: {1}', (localRepoDir, exc))
        print('{0}/{1} - {2}%'.format(counterCurrent,
                                      counterAll, round(counterCurrent*100/counterAll, 2)))
        counterCurrent += 1
    print('\n Creating or Updating repos done')


if __name__ == "__main__":
    main()
